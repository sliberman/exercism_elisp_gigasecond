;;; gigasecond.el --- Gigasecond exercise (exercism)

;;; Commentary:
;; Calculate the date one gigasecond (10^9 seconds) from the
;; given date.
;;
;; NB: Pay attention to  Emacs' handling of time zones and dst
;; in the encode-time and decode-time functions.

;;; Code:

(defun from (seconds minutes hours days months years)
  "Calculate date 1 gigasecond (10^9 seconds from the given date)"
  (butlast
   (let ((encoded-time (encode-time seconds minutes hours days months years t)))
    (decode-time
     (time-add encoded-time 1000000000) t))
   3))





(provide 'gigasecond)
;;; gigasecond.el ends here
